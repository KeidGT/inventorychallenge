/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.applaudo.studios.inventory.sessionBean;

import com.applaudo.studios.inventory.model.Item;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Kevin
 */
@Stateless
public class ItemFacade implements ItemFacadeRemote {

   /*Database Managment emulation [I could't get works persistence unit with h2 :( ]*/
    private static Collection<Item> data = new ArrayList<>();

    @Override
    public Item create(Item item) {
        
        Item old = data.stream()
                    .filter(temp -> item.getItemName().toUpperCase().equals(temp.getItemName().toUpperCase()))
                    .findAny()
                    .orElse(null);
        if(old==null){
            int id = data.size() + 1;
            item.setItemId(id);
            if(item.getItemEnteredDate()==null){
                item.setItemEnteredDate(new Date());
            }
            if(item.getItemLastModifiedDate()==null){
                item.setItemLastModifiedDate(new Date());
            }
            data.add(item);
        }
        
        return item;
    }

    @Override
    public Item find(Integer id) {
        Item item = data.stream()
                .filter(temp -> id.equals(temp.getItemId()))
                .findAny()
                .orElse(null);
        return item;
    }

    @Override
    public List<Item> findAllByStatusAndName(String itemStatus, String itemName) {
        List<Item> result = new ArrayList<>();
        boolean flag = false;
        while (!flag) {
            Item item = data.stream()
                    .filter(temp -> itemStatus.toUpperCase().contains(temp.getItemStatus().toUpperCase()))
                    .filter(temp -> !result.contains(temp))
                    .findAny()
                    .orElse(null);
            if (item != null && !result.contains(item)) {
                result.add(item);
            }
            Item item2 = data.stream()
                    .filter(temp -> itemName.toUpperCase().contains(temp.getItemName().toUpperCase()))
                    .filter(temp -> !result.contains(temp))
                    .findAny()
                    .orElse(null);
            if (item2 != null && !result.contains(item2)) {
                result.add(item2);
            }
            Item item3 = data.stream()
                    .filter(temp -> itemStatus.contains(temp.getItemStatus()))
                    .filter(temp -> !result.contains(temp))
                    .findAny()
                    .orElse(null);
            if (item3 != null && !result.contains(item3)) {
                result.add(item3);
            }
            Item item4 = data.stream()
                    .filter(temp -> itemName.contains(temp.getItemName()))
                    .filter(temp -> !result.contains(temp))
                    .findAny()
                    .orElse(null);
            if (item4 != null && !result.contains(item4)) {
                result.add(item4);
            }
            flag = (item==null && item2==null && item3 ==null);
        }
        return result;
    }
}
