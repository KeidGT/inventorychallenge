/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.applaudo.studios.inventory.singleton;

import com.applaudo.studios.inventory.model.Item;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Kevin
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public @Data
class DataSingleton implements Serializable {

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private static final long serialVersionUID = 685453453453463694L;
   
    private static Collection<Item> data = new ArrayList<>();
    
    @PostConstruct
    public void init() {
       
    }
    
    public void create(Item item) {
        data.add(item);
    }

    public Item find(Object id) {
        Item item = data.stream()
        .filter(temp -> id.equals(temp.getItemId()))
        .findAny()
        .orElse(null);
        return item;
    }

    public List<Item> findAllByStatusAndName(String itemStatus, String itemName) {
        List<Item> result = new ArrayList<>();
        Item item = data.stream()
        .filter(temp -> itemStatus.equals(temp.getItemStatus()))
        .findAny()
        .orElse(null);
        if(item!=null){
            result.add(item);
        }
        Item item2 = data.stream()
        .filter(temp -> itemName.equals(temp.getItemName()))
        .findAny()
        .orElse(null);
        if(item2!=null && !result.contains(item2)){
            result.add(item2);
        }
        return result;
    }

}
