/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.applaudo.studios.inventory.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author Kevin
 */
@Data
@Entity
@Table(name = "item")
public class Item implements Serializable {
    @Id
    @Column(name = "itemId")
    @GeneratedValue(generator = "si")  
    @SequenceGenerator(name="si", sequenceName = "seq_item", allocationSize=1)
    private Integer itemId;
    
    @Size(max = 200)
    @Column(name = "itemName")
    private String itemName;
    
    @Size(max = 200)
    @Column(name = "itemEnteredByUser")
    private String itemEnteredByUser;
    
    @Column(name = "itemEnteredDate", columnDefinition = "TIMESTAMP")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date itemEnteredDate;
    @Column(name = "itemBuyingPrice")
    private Double itemBuyingPrice;
    @Column(name = "itemSellingPrice")
    private Double itemSellingPrice;
    
    @Column(name = "itemLastModifiedDate", columnDefinition = "TIMESTAMP")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date itemLastModifiedDate;
    
    @Size(max = 200)
    @Column(name = "itemLastModifiedByUser")
    private String itemLastModifiedByUser;
    
    @Size(max = 200)
    @Column(name = "itemStatus")
    private String itemStatus;
    
    public Item(){
        
    }

    public Item(Integer itemId) {
        this.itemId = itemId;
    }

    public Item(Integer itemId, String itemName, String itemEnteredByUser, Date itemEnteredDate, Double itemBuyingPrice, Double itemSellingPrice, Date itemLastModifiedDate, String itemLastModifiedByUser, String itemStatus) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemEnteredByUser = itemEnteredByUser;
        this.itemEnteredDate = itemEnteredDate;
        this.itemBuyingPrice = itemBuyingPrice;
        this.itemSellingPrice = itemSellingPrice;
        this.itemLastModifiedDate = itemLastModifiedDate;
        this.itemLastModifiedByUser = itemLastModifiedByUser;
        this.itemStatus = itemStatus;
    }

    public void setId(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
