/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.applaudo.studios.inventory.sessionBean;

import com.applaudo.studios.inventory.model.Item;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Kevin
 */
@Remote
public interface ItemFacadeRemote {

    Item create(Item item);

    Item find(Integer id);
    
    List<Item> findAllByStatusAndName(String itemStatus, String itemName);
    
}
