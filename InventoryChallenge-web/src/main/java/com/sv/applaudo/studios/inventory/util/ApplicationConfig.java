/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sv.applaudo.studios.inventory.util;

import javax.annotation.sql.DataSourceDefinition;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;

/**
 *
 * @author Kevin
 */





@DataSourceDefinition(
		name = "java:app/InventoryDb",
		className = "org.h2.jdbcx.JdbcDataSource",
		url="jdbc:h2:mem:InventoryDb;DB_CLOSE_ON_EXIT=FALSE"
)

@FacesConfig
@ApplicationScoped
public class ApplicationConfig {

}