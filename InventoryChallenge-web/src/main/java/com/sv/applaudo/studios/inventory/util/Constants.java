/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sv.applaudo.studios.inventory.util;

import javax.ws.rs.core.MediaType;

/**
 *
 * @author Kevin
 */
public class Constants {
    
    public static final String 
            DB_URL = "jdbc:h2:mem:InventoryDb", 
            DB_USER = "sa", 
            DB_PASS = "sa", 
            APPLICATION_JSON_DEFAULT = MediaType.APPLICATION_JSON+";charset=utf-8";
}
