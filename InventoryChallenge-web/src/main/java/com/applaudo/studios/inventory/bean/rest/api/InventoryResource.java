package com.applaudo.studios.inventory.bean.rest.api;

import com.applaudo.studios.inventory.model.Item;
import com.applaudo.studios.inventory.sessionBean.ItemFacadeRemote;
import com.sv.applaudo.studios.inventory.util.Constants;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;

/**
 * REST Web Service
 *
 * @author Kevin Guevara
 */
@Stateless
@Path("item")
public class InventoryResource {
    @PostConstruct
    private void posCons() {
        createDatabase();
    }

    @EJB
    ItemFacadeRemote facade; 
    
    @GET
    @Path("{id}")
    @Produces(Constants.APPLICATION_JSON_DEFAULT)
    public Response find(@PathParam("id") Integer id) {
        try {
            
            Item item = facade.find(id);
            
            if(item!=null){
                return Response.ok().entity(item).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(item).build();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(":( ").type(MediaType.APPLICATION_JSON).build();

        }
    }
    
    @GET
    @Produces(Constants.APPLICATION_JSON_DEFAULT)
    public Response find(@QueryParam("itemStatus") String itemStatus, @QueryParam("itemName") String itemName) {
        try {
            
            List<Item> item = facade.findAllByStatusAndName(itemStatus, itemName);
            
            return Response.ok().entity(item).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(":( ").type(MediaType.APPLICATION_JSON).build();

        }
    }
    
    @POST
    @Consumes(Constants.APPLICATION_JSON_DEFAULT)
    @Produces(Constants.APPLICATION_JSON_DEFAULT)
    public Response add(Item entity) {
        try {
            
            
            Item item = facade.create(entity);
            if(item.getItemId()!=null){
                return Response.status(Response.Status.CREATED).entity(item).build();
            }else{
                return Response.status(Response.Status.BAD_REQUEST).entity(item).build();
            }
            

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(":( ").type(MediaType.APPLICATION_JSON).build();

        }
    }
    
    
    private boolean createDatabase() {
        Connection conn = null;
        Statement stmt = null;

        boolean response  = false;
        try {
            
            InputStream temp = InventoryResource.class.getClassLoader().getResourceAsStream("/sql/database.sql");
            String sql = IOUtils.toString(temp, StandardCharsets.UTF_8);
            
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PASS);

            stmt = conn.createStatement();

            int result = stmt.executeUpdate(sql);
            System.out.println("DatabaseCreationResult:" + result);
            response = (result != -1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

}
