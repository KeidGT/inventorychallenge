/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Kevin
 * Created: 02-28-2022
 */

create sequence seq_item;

CREATE TABLE item (

    itemId                    Integer     default seq_item.nextval PRIMARY KEY, 
    itemName                  VARCHAR(200)      ,       
    itemEnteredByUser         VARCHAR(200)      ,      
    itemEnteredDate           TIMESTAMP        ,      
    itemBuyingPrice           Double      ,      
    itemSellingPrice          Double      ,      
    itemLastModifiedDate      Date        ,                   
    itemLastModifiedByUser    VARCHAR(200)      ,               
    itemStatus                VARCHAR(200)             

  
);

insert into item(itemName, itemEnteredByUser, itemEnteredDate, 
itemBuyingPrice, itemLastModifiedDate, itemLastModifiedByUser, itemStatus) 
values('Suggar', 'kguevara', CURRENT_TIMESTAMP(), 5.50, CURRENT_TIMESTAMP(), 'kguevara', 'AVAILABLE');
